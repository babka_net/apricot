Apricot: An easy way to manage your Fediverse Allow/Blocklists
===============================================================

- [About](#About)
- [Quickstart](#Quickstart)

About
------

Managing Fediverse Block/Allow lists can be a pain for a Fediverse admin, but Apricot makes it easy to manage your lists and share them with others, making for a safer Fediverse.

Apricot can work with existing tools, or on its own to allow you to easily manage your lists, including categorization, easy import/export, DNSRBL lists, and more.

Installation
-------------
TBD

Quickstart
-----------

This is a very quick example of using Apricot. For more elaborate configuration, please read the [Tutorial](/tutorial).

Let's start with some defaults to get us started. Begin by moving the sample configuration into our configuration and creating a directory for our entries.

```
mv config.toml.sample config.toml
mkdir entries
```

Now you can simply add an entry. Let's create one for example.com, saying that we're banning it spam.

```
poetry run add_entry example.com spam
```

Now we can simply run apricot's run process and we'll have files in our `output` directory we can use!

```
poetry run build
```

This merely scratches the surface. We go into much more depth in the [Tutorial](/tutorial).

