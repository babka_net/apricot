import logging
from flask import Flask, render_template, jsonify, make_response
from io import StringIO
import csv
import toml
from .db import load_db

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.NOTSET)

app = Flask(__name__)

def create_app():
    app = Flask(__name__)
    app.config.from_file("config.toml", load=toml.load)
    with app.app_context():
        app.db = load_db(directory=app.config['ENTRIES_DIRECTORY'],
                         categories=app.config['CATEGORIES'])
    return app

app = create_app()

@app.route('/blocklist.txt')
def blocklist_all():
    resp = make_response(render_template('blocklist.txt',
                                         entries=app.db.entries.values()))
    resp.headers['Content-type'] = 'text/plain; charset=utf-8'
    return resp

@app.route('/domain_blocks.csv')
def domain_blocks():
    """Return a Mastodon style csv file"""
    fd = StringIO()
    cw = csv.writer(fd)
    header = ['#domain', '#severity', '#reject_media', '#reject_reports',
              '#public_comment', '#obfuscate']
    cw.writerow(header)
    for entry in app.db.entries.values():
        if not entry.type == 'domain':
            continue
        comment = entry.comment or ':'.join(entry.reasons)
        row = [entry.address, entry.action, 'false', 'false', comment,
               entry.private]
        cw.writerow(row)

    output = make_response(fd.getvalue())
    output.headers["Content-Disposition"] = \
        "attachment; filename=domain_blocks.csv"
    output.headers["Content-type"] = "text/csv"
    return output

@app.route('/blocklists/<reason>.txt')
def blocklist(reason):
    entries = app.db.reasons[reason]
    resp = make_response(render_template('blocklist.txt', entries=entries))
    resp.headers['Content-type'] = 'text/plain; charset=utf-8'
    return resp

