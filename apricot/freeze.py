from flask_frozen import Freezer
from .app import app

freezer = Freezer(app)
app.config['FREEZER_DESTINATION'] = '../public'
def freeze():
    freezer.freeze()

@freezer.register_generator
def blocklist():
    for reason in app.db.reasons:
        yield {'reason': reason}

if __name__ == '__main__':
    freezer.freeze()
