from collections import defaultdict
from dataclasses import dataclass, field
from typing import Optional
from datetime import datetime, timedelta
from typing import Optional
from pathlib import Path
import toml

@dataclass
class Entry:
    address: str # IP Range or domain
    created_at: Optional[datetime]
    type: str = 'domain' # TODO: Use config
    subdomain: str = "include"
    reasons: list[str]= field(default_factory=list)
    comment: str = ""
    action: str = 'suspend' # TODO: Use config
    # reject_reports?
    # reject_media?
    private = bool = False # TODO: Use config

@dataclass
class DB:
    entries: dict[Entry] = field(default_factory=dict)
    reasons: defaultdict[Entry] = field(default_factory=dict)

def load_db(directory, categories):
    """Load the 'database' of entries"""
    entries = {}
    reasons = defaultdict(list)
    
    entry_files = [f for f in Path(directory) if f.is_file()]

    for fname in entry_files:
        with open(fname, 'r') as fp:
            data = toml.load(fp)
            entry = Entry(address = data['address'],
                          subdomain = data.get('subdomain'),
                          created_at = data.get('created_at')
                          )

            raw_reason = data['reason']
            if isinstance(raw_reason, list):
                entry.reasons = raw_reason
            elif isinstance(raw_reason, str):
                entry.reasons = [raw_reason]
            # Validate reasons!

            entries[entry.address] = entry
            for cat in entry.reasons:
                if cat not in categories:
                    raise ValueError(f"{fname} lists unsupported category {cat}")
                reasons[cat].append(entry)

    return DB(entries=entries, reasons = reasons)
